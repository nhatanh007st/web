import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';

import { log } from 'util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  public Data;
  private urlData = 'http://localhost:3000/users';
  private urltest = 'http://localhost:3000/';

  constructor(private http: Http) {
    this.getData();
  }

  getData() {
    let body = {
      "name": "Nhat Anh"
    };

    this.http.get(this.urlData)
      .toPromise()
      .then(response => {
        this.Data = response.json().body;
        console.log(this.Data);
        return this.Data;
      })
      .catch(error => {
        console.log(error);
        return Promise.reject(error);
      });
  }

}
